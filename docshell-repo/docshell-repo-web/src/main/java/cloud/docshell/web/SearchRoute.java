package cloud.docshell.web;

import java.io.IOException;
import java.util.List;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.ironlist.IronList;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;

import cloud.docshell.repo.index.DocshellResult;
import cloud.docshell.web.data.Shared;

@SuppressWarnings("serial")
@CssImport("styles/search.css")
@Route(value = "search", layout = DocShellWebLayout.class)
public class SearchRoute extends VerticalLayout implements HasUrlParameter<String> {

	private final Label resultsCount;
	private final IronList<DocshellResult> list;

	public SearchRoute() {
		this.setWidthFull();

		this.resultsCount = new Label();
		this.resultsCount.getStyle().set("font-size", "small");
		this.resultsCount.getStyle().set("font-style", "italic");

		this.list = new IronList<>();
		this.list.setRenderer(TemplateRenderer.<DocshellResult> of(
				"<div class=\"result\">"
						+ "<div class=\"labels\">"
						+ "<label class=\"title\"><a href=\"[[item.url]]\" target=\"_blank\">[[item.title]]</a></label>"
						+ "<span class=\"detail\"><iron-icon icon=\"vaadin:pin\"></iron-icon> [[item.version]]</span>"
						+ "<span class=\"detail\"><iron-icon icon=\"vaadin:user\"></iron-icon> [[item.author]]</span>"
						+ "</div>"
						+ "<div>"
						+ "<vaadin-button title=\"Edit this configuration\" class=\"button\" on-click=\"edit\" theme=\"primary icon\"><iron-icon icon=\"vaadin:edit\" slot=\"prefix\"></iron-icon></vaadin-button>"
						+ "<vaadin-button title=\"Delete this configuration\" class=\"button\" on-click=\"delete\" theme=\"primary error icon\"><iron-icon icon=\"vaadin:trash\" slot=\"prefix\"></iron-icon></vaadin-button>"
						+ "</div>"
						+ "</div>")
				.withProperty("title", DocshellResult::getTitle)
				.withProperty("url", d -> d.getUrl() == null ? "" : d.getUrl().toString())
				.withProperty("date", DocshellResult::getVersion)
				.withProperty("author", DocshellResult::getAuthor)
				.withEventHandler("delete", i -> System.out.println(i)));
		this.list.setSizeFull();
		this.add(this.resultsCount, this.list);
	}

	@Override
	public void setParameter(final BeforeEvent event, @OptionalParameter final String parameter) {
		if (parameter == null || parameter.isBlank()) {
			this.list.setItems(List.of());
			this.resultsCount.setText("No search query provided");
		} else {
			try {
				final List<DocshellResult> results = Shared.INDEXER.query(parameter);
				this.list.setItems(results);
				this.resultsCount.setText("Results found: " + results.size() + " for query '" + parameter + "'");
			} catch (final IOException ex) {
				ex.printStackTrace();
				this.resultsCount.setText("Error during search");
			}
		}
	}
}
