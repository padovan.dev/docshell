package cloud.docshell.web;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.vaadin.flow.component.html.IFrame;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.Route;

import cloud.docshell.schema.docshell.Docshell;

@SuppressWarnings("serial")
@ParentLayout(DocShellWebLayout.class)
@Route(value = "tree", layout = DocShellWebLayout.class)
public class TreeRoute extends VerticalLayout implements HasUrlParameter<String> {

	private final Logger logger = System.getLogger(TreeRoute.class.getName());

	private final MemoryBuffer uploadBuffer;
	private final Upload upload;
	private final IFrame frame;
	private final TreeGrid<Node> tree;

	private Docshell docshell;
	private Element documentElement;

	public TreeRoute() {

		this.setPadding(false);
		this.setSpacing(false);
		this.setSizeFull();

		this.uploadBuffer = new MemoryBuffer();
		this.upload = new Upload(this.uploadBuffer);
		this.frame = new IFrame("https://www.docshell.cloud/bash1.xml?pippo=aa&pluto=ff");
		this.frame.setSizeFull();
		this.frame.getStyle().set("border", "none");

		this.tree = new TreeGrid<>();
		this.tree.setSizeFull();
		this.tree.addHierarchyColumn(Node::getNodeName);
		this.tree.addComponentColumn(node -> {
			System.out.println(node.getUserData("path"));
			final TextField field = new TextField();
			field.setValue(StringUtils.defaultIfEmpty(node.getNodeValue(), ""));
			field.addValueChangeListener(e -> node.setNodeValue(StringUtils.defaultIfEmpty(e.getValue(), null)));
			return field;
		}).setHeader("Value");
		this.tree.addColumn(node -> {
			final NamedNodeMap attributes = node.getAttributes();
			return IntStream.range(0, attributes.getLength()).mapToObj(attributes::item)
					.map(n -> n.getNodeName() + "=" + n.getNodeValue())
					.collect(Collectors.joining(", "));
		}).setHeader("Attributes");
		this.tree.addColumn(node -> {
			final NodeList nodeList = node.getChildNodes();
			return IntStream.range(0, nodeList.getLength()).mapToObj(nodeList::item)
					.filter(node1 -> node1.getNodeType() == Node.TEXT_NODE)
					.map(node1 -> node1.getNodeValue())
					.filter(Objects::nonNull)
					.collect(Collectors.joining(" "));
		}).setHeader("Text");

		this.upload.addSucceededListener(event -> {
			this.onLoad(this.uploadBuffer.getInputStream());
		});

		this.add(this.upload);
		// this.addAndExpand(this.frame);
		this.addAndExpand(this.tree);
	}

	private void onLoad(final InputStream is) {
		try (is) {
			// final Unmarshaller unmarshaller = JAXBContext.newInstance(Docshell.class).createUnmarshaller();
			// final Object obj = unmarshaller.unmarshal(is);
			// final Docshell ds = (Docshell) (obj instanceof JAXBElement ? ((JAXBElement<?>) obj).getValue() : obj);
			// this.load(ds);
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();
			final Document document = builder.parse(is);
			document.getDocumentElement().normalize();
			this.documentElement = document.getDocumentElement();
			this.documentElement.setUserData("path", Path.of("/" + this.documentElement.getNodeName()), null);
			this.tree.setItems(Stream.of(this.documentElement), node -> {
				final NodeList nodeList = node.getChildNodes();
				final Path path = (Path) node.getUserData("path");
				return IntStream.range(0, nodeList.getLength())
						.mapToObj(nodeList::item)
						.filter(node1 -> node1.getNodeType() == Node.ELEMENT_NODE)
						.peek(node1 -> node1.setUserData("path", path.resolve(node1.getNodeName()), null));
			});
		} catch (final Exception ex) {
			this.logger.log(Level.ERROR, ex);
		}
	}

	private void load(final Docshell ds) {
		this.docshell = ds;
	}

	@Override
	public void setParameter(final BeforeEvent event, final @OptionalParameter String parameter) {
		// TODO Auto-generated method stub

	}
}
