package cloud.docshell.web.common;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.IntStream;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.timepicker.TimePicker;

@SuppressWarnings("serial")
public class OffsetDateTimeField extends CustomField<OffsetDateTime> {

	private final DatePicker datePicker = new DatePicker();
	private final TimePicker timePicker = new TimePicker();
	private final Select<Integer> offset = new Select<>(IntStream.rangeClosed(-12, +12).mapToObj(Integer::valueOf).toArray(Integer[]::new));
	private OffsetDateTime value;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public OffsetDateTimeField() {
		this.timePicker.setWidth("6em");
		this.offset.setWidth("5em");
		this.add(new HorizontalLayout(this.datePicker, this.timePicker, this.offset));

		this.offset.setValue(Integer.valueOf(0));
		this.offset.setEmptySelectionAllowed(false);
		this.offset.setItemLabelGenerator(val -> (val.intValue() >= 0 ? "+" : "-") + String.format("%02d", Integer.valueOf(Math.abs(val.intValue()))));

		final ValueChangeListener listener = e -> {
			if (e.isFromClient()) {
				this.updateFromFields();
			}
		};
		this.datePicker.addValueChangeListener(e -> {
			if (e.getValue() != null) {
				if (this.timePicker.isEmpty()) {
					this.timePicker.setValue(LocalTime.now());
				}
			} else {
				this.timePicker.clear();
			}
		});
		this.timePicker.addValueChangeListener(e -> {
			if (e.getValue() != null) {
				if (this.datePicker.isEmpty()) {
					this.datePicker.setValue(LocalDate.now());
				}
			} else {
				this.datePicker.clear();
			}
		});
		this.timePicker.addValueChangeListener(listener);
		this.datePicker.addValueChangeListener(listener);
	}

	public OffsetDateTimeField(final String label) {
		this();
		this.setLabel(label);
	}

	@Override
	protected OffsetDateTime generateModelValue() {
		return this.value;
	}

	@Override
	protected void setPresentationValue(final OffsetDateTime newValue) {
		this.datePicker.setValue(newValue.toLocalDate());
		this.timePicker.setValue(newValue.toLocalTime());
		this.offset.setValue(Integer.valueOf(newValue.getOffset().getTotalSeconds() / 3600)); // FIXME maybe it's incorrect
		this.value = newValue;
	}

	private void updateFromFields() {
		try {
			this.value = OffsetDateTime.of(this.datePicker.getValue(), this.timePicker.getValue(), ZoneOffset.ofHours(this.offset.getValue().intValue()));
		} catch (@SuppressWarnings("unused") final Exception ex) {
			this.value = null;
		}
	}
}