package cloud.docshell.web;

import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.vaadin.firitin.components.button.VButton;
import org.vaadin.firitin.components.formlayout.VFormLayout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.IFrame;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.StreamResourceRegistry;
import com.vaadin.flow.server.VaadinSession;

import cloud.docshell.schema.docshell.Command;
import cloud.docshell.schema.docshell.Docshell;
import cloud.docshell.schema.docshell.Io;
import cloud.docshell.schema.docshell.IoType;
import cloud.docshell.schema.docshell.Param;
import cloud.docshell.web.common.OffsetDateTimeField;
import cloud.docshell.web.data.DocshellFactory;

@SuppressWarnings("serial")
@Route(value = "edit", layout = DocShellWebLayout.class)
public class EditorRoute extends SplitLayout implements HasUrlParameter<String>, BeforeLeaveObserver {

	private static final Logger LOGGER = System.getLogger(EditorRoute.class.getSimpleName());

	private Docshell docshell;
	private StreamRegistration xmlResourceRegistration;
	private StreamRegistration xslResourceRegistration;

	private final IFrame previewFrame;

	private final Binder<Docshell> binder;
	private final TextField title;
	private final TextField author;
	private final OffsetDateTimeField date;
	private final TextField version;
	private final TextArea tags;

	private final Grid<Command> commandList;

	public EditorRoute() {
		this.setSizeFull();

		this.binder = new Binder<>();

		this.title = new TextField();
		this.title.setWidthFull();
		this.binder.forField(this.title).bind(Docshell::getTitle, Docshell::setTitle);

		this.author = new TextField();
		this.binder.forField(this.author).bind(Docshell::getAuthor, Docshell::setAuthor);

		this.date = new OffsetDateTimeField();
		this.binder.forField(this.date).bind(Docshell::getDate, Docshell::setDate);

		this.version = new TextField();
		this.binder.forField(this.version).bind(Docshell::getVersion, Docshell::setVersion);

		this.tags = new TextArea();
		this.binder.forField(this.tags).bind(
				ds -> ds.getTag().stream().collect(Collectors.joining("\n")),
				(ds, value) -> {
					ds.getTag().clear();
					ds.getTag().addAll(value.isEmpty() ? List.of() : List.of(value.split("\\n")));
				});

		final VFormLayout form = new VFormLayout();
		form.addFormItem(this.title, "Title", 2);
		form.addFormItem(this.author, "Author");
		form.addFormItem(this.date, "Date", 2);
		form.addFormItem(this.version, "Version");
		form.addFormItem(this.tags, "Tags");

		final Button parameters = new Button("Declare parameters ...");
		parameters.addClickListener(e -> this.declareParameters());
		form.add(parameters);

		final Details generalInformations = new Details("General information", form);
		generalInformations.setOpened(true);

		final VerticalLayout leftSide = new VerticalLayout();
		leftSide.setWidthFull();
		final Div leftWrapper = new Div(leftSide);
		leftWrapper.setSizeFull();
		this.addToPrimary(leftWrapper);

		leftSide.add(generalInformations);

		this.commandList = new Grid<>();
		this.commandList.addColumn(new ComponentRenderer<>(this::renderCommand));
		this.commandList.setHeightByRows(true);
		leftSide.add(this.commandList);

		this.previewFrame = new IFrame();
		this.previewFrame.setWidthFull();
		this.previewFrame.setId("preview");
		this.previewFrame.getStyle().set("border", "none");
		this.previewFrame.setSizeFull();

		final Div rightWrapper = new Div(this.previewFrame);
		rightWrapper.setSizeFull();
		rightWrapper.getStyle().set("overflow", "hidden"); // XXX
		this.addToSecondary(rightWrapper);

		this.binder.addValueChangeListener(e -> this.triggerChanges());

		// UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
		// final int timezoneOffset = details.getTimezoneOffset();
		// try {
		// this.clientOffset = ZoneOffset.ofTotalSeconds(timezoneOffset / 1000);
		// } catch (@SuppressWarnings("unused") final DateTimeException ex) {
		// LOGGER.log(Level.ERROR, "Cannot determine client timezone offset {0}", "" + timezoneOffset);
		// this.clientOffset = ZoneOffset.UTC;
		// }
		// });
	}

	private void triggerChanges() {
		this.getUI().ifPresent(ui -> ui.getPage().executeJs("let frame = document.getElementById('preview');frame.src = frame.src;"));
	}

	private void declareParameters() {
		final Grid<Param> gridParams = new Grid<>();
		gridParams.setWidth("700px");
		gridParams.addComponentColumn(param -> {
			final TextField name = new TextField();
			name.setWidthFull();
			name.addThemeVariants(TextFieldVariant.LUMO_SMALL);
			name.setValue(Objects.toString(param.getName(), ""));
			name.addValueChangeListener(e -> param.setName(StringUtils.stripToNull(e.getValue())));
			return name;
		}).setHeader("Name").setWidth("8em").setFlexGrow(0);
		gridParams.addComponentColumn(param -> {
			final TextField defaultValue = new TextField();
			defaultValue.setWidthFull();
			defaultValue.addThemeVariants(TextFieldVariant.LUMO_SMALL);
			defaultValue.setValue(Objects.toString(param.getDefaultValue(), ""));
			defaultValue.addValueChangeListener(e -> param.setDefaultValue(StringUtils.stripToNull(e.getValue())));
			return defaultValue;
		}).setHeader("Default").setWidth("8em").setFlexGrow(0);
		// gridParams.addComponentColumn(param -> {
		// final TextField currentValue = new TextField();
		// currentValue.setWidthFull();
		// currentValue.addThemeVariants(TextFieldVariant.LUMO_SMALL);
		// currentValue.addValueChangeListener(e -> {});
		// return currentValue;
		// }).setHeader("Current").setWidth("8em").setFlexGrow(0);
		gridParams.addComponentColumn(param -> {
			final TextField description = new TextField();
			description.setWidthFull();
			description.addThemeVariants(TextFieldVariant.LUMO_SMALL);
			description.setValue(Objects.toString(param.getDescription(), ""));
			description.addValueChangeListener(e -> param.setDescription(StringUtils.stripToNull(e.getValue())));
			return description;
		}).setHeader("Description").setFlexGrow(1);
		gridParams.addColumn(TemplateRenderer.<Param> of("<vaadin-button theme='small icon error primary' on-click='delete'><iron-icon icon='vaadin:trash' slot='prefix'></iron-icon></vaadin-button>")
				.withEventHandler("delete", param -> {
					this.docshell.getParam().remove(param);
					gridParams.getDataProvider().refreshAll();
				})).setFlexGrow(0).setHeader(new VButton(VaadinIcon.PLUS.create(), event -> {
					this.docshell.getParam().add(new Param());
					gridParams.getDataProvider().refreshAll();
				}).withThemeVariants(
						ButtonVariant.LUMO_SMALL,
						ButtonVariant.LUMO_PRIMARY,
						ButtonVariant.LUMO_SUCCESS,
						ButtonVariant.LUMO_ICON));
		gridParams.setItems(this.docshell.getParam());
		gridParams.setSelectionMode(SelectionMode.NONE);
		gridParams.getDataProvider().addDataProviderListener(event -> this.triggerChanges());

		final Dialog dialog = new Dialog(gridParams);
		dialog.open();
	}

	@Override
	public void setParameter(final BeforeEvent event, final @OptionalParameter String parameter) {
		if (parameter == null || parameter.isEmpty()) {
			this.docshell = DocshellFactory.newDocshell();
		} else {
			try (InputStream is = Files.newInputStream(Path.of(""))) {
				final Unmarshaller unmarshaller = JAXBContext.newInstance(Docshell.class).createUnmarshaller();
				this.docshell = (Docshell) unmarshaller.unmarshal(is);
			} catch (final IOException | JAXBException ex) {
				ex.printStackTrace();
				event.rerouteToError(new IllegalArgumentException(), "Invalid parameter");
				return;
			}
		}
		this.prepareResource();
		this.binder.setBean(this.docshell);
		this.commandList.setItems(this.docshell.getCommand());
	}

	@Override
	public void beforeLeave(final BeforeLeaveEvent event) {
		this.xmlResourceRegistration.unregister();
		this.xslResourceRegistration.unregister();
	}

	private void prepareResource() {
		final StreamResource xslresource = new StreamResource("style.xsl", () -> this.getClass().getResourceAsStream("/styles/docshell.xsl"));
		xslresource.setContentType("text/xsl");

		final StreamResourceRegistry resourceRegistry = VaadinSession.getCurrent().getResourceRegistry();
		this.xslResourceRegistration = resourceRegistry.registerResource(xslresource);

		final StreamResource xmlresource = new StreamResource("temp.xml", (stream, session) -> {
			try {
				final Marshaller marshaller = JAXBContext.newInstance(Docshell.class).createMarshaller();
				marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml-stylesheet type=\"text/xsl\" href=\"/" + this.xslResourceRegistration.getResourceUri() + "\"?>");
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				marshaller.marshal(this.docshell, stream);
			} catch (final JAXBException ex) {
				LOGGER.log(Level.ERROR, "Error marshalling xml", ex);
			}
		});
		xmlresource.setContentType("application/xml");
		xmlresource.setCacheTime(0);
		this.xmlResourceRegistration = resourceRegistry.registerResource(xmlresource);
		this.previewFrame.setSrc(this.xmlResourceRegistration.getResourceUri().toString());
	}

	private Component renderCommand(final Command command) {

		final TextArea comment = new TextArea();
		comment.setWidthFull();
		comment.setPlaceholder("Command comment");

		final Grid<Io> ioList = new Grid<>();
		ioList.addColumn(new ComponentRenderer<>(io -> this.renderIo(io, command, ioList)));
		ioList.setItems(command.getIo());
		ioList.getDataProvider().addDataProviderListener(e -> this.triggerChanges());
		ioList.setHeightByRows(true);

		final Button btnAddIo = new VButton("Add", VaadinIcon.PLUS.create())
				.withThemeVariants(ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_SMALL)
				.withClickListener(event -> {
					command.getIo().add(DocshellFactory.newIo());
					ioList.getDataProvider().refreshAll();
				});

		return new VerticalLayout(comment, ioList, btnAddIo);
	}

	private Component renderIo(final Io io, final Command command, final Grid<Io> ioList) {
		final TextArea area = new TextArea();
		area.setWidthFull();

		final FlexLayout buttons = new FlexLayout();
		buttons.getStyle().set("flex-direction", "column");
		final Button btnInputType = new Button((IoType.INPUT == io.getType() ? VaadinIcon.ANGLE_DOUBLE_RIGHT : VaadinIcon.ANGLE_DOUBLE_LEFT).create());
		btnInputType.addThemeVariants(ButtonVariant.LUMO_SMALL);
		btnInputType.addClickListener(e -> {

		});
		final Button btnAdd = new Button(VaadinIcon.PLUS.create());
		btnAdd.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_SUCCESS);
		btnAdd.addClickListener(e -> {
			final int ix = command.getIo().indexOf(io);
			command.getIo().add(ix + 1, DocshellFactory.newIo());
			ioList.getDataProvider().refreshAll();
		});
		final Button btnDelete = new Button(VaadinIcon.TRASH.create());
		btnDelete.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_ERROR);
		btnDelete.addClickListener(e -> {
			command.getIo().remove(io);
			ioList.getDataProvider().refreshAll();
		});
		buttons.add(btnDelete, btnAdd);

		area.setPrefixComponent(btnInputType);
		area.setSuffixComponent(buttons);
		area.getStyle().set("font-size", "small");
		area.getStyle().set("font-family", "Monospace"); // TODO use css and a font list for other os
		// area.getStyle().set("background", IoType.INPUT == io.getType() ? "#c8e6c9" : "#ffccbc");
		area.setValue(Objects.requireNonNullElse(io.getValue(), ""));
		area.addValueChangeListener(e -> io.setValue(StringUtils.defaultIfEmpty(e.getValue(), null)));
		return area;
	}
}
