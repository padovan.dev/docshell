package cloud.docshell.web.data;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.Random;

import cloud.docshell.schema.docshell.Command;
import cloud.docshell.schema.docshell.Docshell;
import cloud.docshell.schema.docshell.Io;
import cloud.docshell.schema.docshell.IoType;
import cloud.docshell.schema.docshell.Param;

public class DocshellFactory {

	public static Docshell newDocshell() {
		final Docshell docshell = new Docshell();
		docshell.setTitle("New document");
		docshell.setAuthor("Mr. White");
		docshell.setDate(OffsetDateTime.now());
		docshell.setVersion("1");

		final Param param = new Param();
		param.setName("Server");
		param.setDefaultValue("localhost");
		param.setDescription("The server where this command will be executed");
		docshell.getParam().add(param);
		Collections.addAll(docshell.getTag(), "Foo", "Bar");

		final Random random = new Random();

		for (int i = 0; i < 5; i++) {
			final Command command = new Command();
			command.setShellType("BASH");
			docshell.getCommand().add(command);

			for (int j = 0; j < random.nextInt(5) + 1; j++) {
				final Io io = new Io();
				io.setValue(j % 2 == 0 ? "echo 'Hello World " + j / 2 + "'" : "Hello");
				io.setType(j % 2 == 0 ? IoType.INPUT : IoType.OUTPUT);
				command.getIo().add(io);
			}
		}

		return docshell;
	}

	public static Io newIo() {
		final Io io = new Io();
		io.setType(IoType.INPUT);
		return io;
	}

}
