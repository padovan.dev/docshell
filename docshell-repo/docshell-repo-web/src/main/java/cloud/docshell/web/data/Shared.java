package cloud.docshell.web.data;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

import cloud.docshell.repo.index.Indexer;

public final class Shared {

	public static final Indexer INDEXER;
	static {
		try {
			INDEXER = new Indexer(Path.of(Objects.requireNonNull(System.getProperty("cloud.docshell.index.path"), "Property 'cloud.docshell.index.path' must be set")));
		} catch (final IOException ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}
}
