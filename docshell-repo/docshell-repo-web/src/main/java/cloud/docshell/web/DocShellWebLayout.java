package cloud.docshell.web;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.page.LoadingIndicatorConfiguration;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.server.InitialPageSettings;
import com.vaadin.flow.server.PageConfigurator;

@SuppressWarnings("serial")
@CssImport("./styles/styles.css")
@JsModule("./styles/shared-styles.js")
// @PWA(name = "Project Base for Vaadin", shortName = "Project Base")
public class DocShellWebLayout extends AppLayout implements AfterNavigationObserver, PageConfigurator {

	private final TextField searchField;
	// private final Button newDocument;

	public DocShellWebLayout() {

		// final Div logo = new Div();
		// logo.setWidth("256px");
		// this.addToNavbar(logo);

		this.searchField = new TextField();
		this.searchField.setPrefixComponent(VaadinIcon.SEARCH.create());
		this.searchField.setPlaceholder("Find ...");
		this.searchField.setWidthFull();
		this.searchField.setValueChangeMode(ValueChangeMode.ON_CHANGE);
		this.searchField.addValueChangeListener(e -> {
			if (e.isFromClient()) {
				this.getUI().ifPresent(ui -> ui.navigate(SearchRoute.class, e.getValue()));
			}
		});
		this.addToNavbar(this.searchField);
		//
		// this.newDocument = new Button("Create document", VaadinIcon.FILE_ADD.create());
		// this.newDocument.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_LARGE);
		// this.newDocument.setWidthFull();
		// this.newDocument.addClickListener(e -> {
		// try {
		// Thread.sleep(10000);
		// } catch (final InterruptedException ex) {
		// ex.printStackTrace();
		// }
		// });
		// this.addToDrawer(this.newDocument);

	}

	@Override
	public void afterNavigation(final AfterNavigationEvent event) {
		final String path = event.getLocation().getPath();
		if (path.startsWith("search/")) {
			final String searchStr = path.substring(7);
			this.searchField.setValue(searchStr);
		} else {
			this.searchField.clear();
		}
	}

	@Override
	public void configurePage(final InitialPageSettings settings) {
		final LoadingIndicatorConfiguration conf = settings.getLoadingIndicatorConfiguration();

		// disable default theme -> loading indicator will not be shown
		conf.setApplyDefaultTheme(false);
	}
}
