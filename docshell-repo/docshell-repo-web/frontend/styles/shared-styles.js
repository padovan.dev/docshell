import '@polymer/polymer/lib/elements/custom-style.js';

const documentContainer = document
        .createElement('template');

documentContainer.innerHTML = `
<dom-module id="my-lumo-button" theme-for="vaadin-app-layout">
	<template>
		<style>
			[part="navbar"] {
				background-color: var(--lumo-primary-color-50pct);
			}
			[part="navbar"]::before {
				opacity: 0;
			}
		</style>
	</template>
</dom-module>`;

document.head.appendChild(documentContainer.content);