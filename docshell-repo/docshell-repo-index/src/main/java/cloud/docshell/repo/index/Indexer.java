package cloud.docshell.repo.index;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.QueryBuilder;

import cloud.docshell.schema.docshell.Command;
import cloud.docshell.schema.docshell.Comment;
import cloud.docshell.schema.docshell.Docshell;
import cloud.docshell.schema.docshell.Io;
import cloud.docshell.schema.docshell.IoType;
import cloud.docshell.schema.docshell.Os;

public class Indexer implements AutoCloseable {

	public static void main(final String[] args) throws MalformedURLException, IOException, JAXBException {
		final Path path = Paths.get("/tmp/docshell");
		if (!Files.exists(path)) {
			Files.createDirectories(path);
		}
		final boolean index = true;
		try (final Indexer indexer = new Indexer(path)) {
			if (index) {
				indexer.index(
						new URL("https://www.docshell.cloud/repo/padovan.dev/ssh-key.xml"),
						new URL("https://www.docshell.cloud/bash1.xml"));
			}
			final List<DocshellResult> query = indexer.query("ubuntu");
			for (final DocshellResult docshellResult : query) {
				System.out.println(docshellResult.getId() + "\t" + docshellResult.getTitle() + "\t" + docshellResult.getScore());
			}
		}
	}

	private final Path indexDir;
	private final Directory index;
	private final StandardAnalyzer analyzer = new StandardAnalyzer();
	private IndexReader reader;

	public Indexer(final Path indexDir) throws IOException {
		this.indexDir = indexDir;
		this.index = new SimpleFSDirectory(this.indexDir);
	}

	@Override
	public void close() {
		this.analyzer.close();
	}

	public List<DocshellResult> query(final String queryText) throws IOException {

		final QueryBuilder queryBuilder = new QueryBuilder(this.analyzer);
		final Query title = queryBuilder.createPhraseQuery("title", queryText);
		final Query comment = queryBuilder.createPhraseQuery("comment", queryText);
		final Query commandInput = queryBuilder.createPhraseQuery("command-input", queryText);
		final Query commandOutput = queryBuilder.createPhraseQuery("command-output", queryText);

		final BooleanQuery q = new BooleanQuery.Builder()
				.add(title, Occur.SHOULD)
				.add(comment, Occur.SHOULD)
				.add(commandInput, Occur.SHOULD)
				.add(commandOutput, Occur.SHOULD)
				.build();

		final int hitsPerPage = 10;
		final IndexSearcher searcher = new IndexSearcher(this.getIndexReader());
		final TopDocs docs = searcher.search(q, hitsPerPage);
		final ScoreDoc[] hits = docs.scoreDocs;
		final List<DocshellResult> result = new ArrayList<>(hits.length);
		for (final ScoreDoc hit : hits) {
			final int docId = hit.doc;
			final Document d = searcher.doc(docId);
			final Set<String> tags = new HashSet<>(Arrays.asList(d.getValues("tag")));
			result.add(new DocshellResult(d.get("id"), new URL(d.get("url")), d.get("title"), d.get("author"), d.get("version"), tags, hit.score));
		}
		return result;
	}

	private synchronized IndexReader getIndexReader() throws IOException {
		if (this.reader == null) {
			this.reader = DirectoryReader.open(this.index);
		}
		return this.reader;
	}

	public synchronized void index(final URL... docshells) throws IOException, JAXBException {
		final IndexWriterConfig config = new IndexWriterConfig(this.analyzer);
		try (final IndexWriter w = new IndexWriter(this.index, config)) {
			try {
				for (final URL url : docshells) {
					addDoc(w, url);
				}
			} catch (final IOException | JAXBException ex) {
				w.rollback();
				throw ex;
			}

		}
	}

	private static void addDoc(final IndexWriter w, final URL url) throws IOException, JAXBException {
		Docshell ds;
		final JAXBContext jaxbContext = JAXBContext.newInstance(Docshell.class);
		try (InputStream is = url.openStream()) {
			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			ds = (Docshell) unmarshaller.unmarshal(is);
			addDoc(w, url, ds);
		}
	}

	private static void addDoc(final IndexWriter w, final URL url, final Docshell ds)
			throws IOException {
		final Document doc = new Document();
		doc.add(new StringField("id", ds.getId(), Field.Store.YES));
		doc.add(new StringField("url", url.toExternalForm(), Field.Store.YES));
		doc.add(new TextField("title", ds.getTitle(), Field.Store.YES));
		doc.add(new StringField("author", ds.getAuthor(), Field.Store.YES));
		doc.add(new StringField("version", ds.getVersion(), Field.Store.YES));
		doc.add(new LongPoint("date", ds.getDate().toInstant().toEpochMilli()));

		final Os os = ds.getOs();
		if (os != null) {
			final String family = os.getFamily();
			addField(doc, "os-family", family, false);
			final String distribution = os.getDistribution();
			addField(doc, "os-distribution", distribution, false);
			final String version = os.getVersion();
			addField(doc, "os-version", version, false);
		}
		for (final String tag : ds.getTag()) {
			doc.add(new TextField("tag", tag, Field.Store.YES));
		}
		for (final Comment comment : ds.getComment()) {
			addField(doc, "comment", comment.getValue(), false);
		}
		for (final Command command : ds.getCommand()) {
			final String comment = Optional.ofNullable(command.getComment()).map(Comment::getValue).orElse(null);
			addField(doc, "command-comment", comment, false);
			for (final Io io : command.getIo()) {
				final IoType type = io.getType();
				switch (type) {
					case INPUT:
						addField(doc, "command-input", io.getValue(), false);
						break;
					case OUTPUT:
						addField(doc, "command-output", io.getValue(), false);
						break;
					case BASE_64:
					case DIAGRAM:
					case FILECONTENT:
					default:
						break;
				}
				for (final Comment ioComment : io.getComment()) {
					addField(doc, "command-io-comment", ioComment.getValue(), false);
				}
			}
		}
		w.addDocument(doc);
	}

	private static void addField(final Document doc, final String key, final String value, final boolean store) {
		if (value != null && !value.isBlank()) {
			doc.add(new TextField(key, value, store ? Field.Store.YES : Field.Store.NO));
		}
	}

}
