package cloud.docshell.repo.index;

import java.io.Serializable;
import java.net.URL;
import java.util.Set;

public class DocshellResult implements Serializable {

	private static final long serialVersionUID = 7791499271481605010L;

	private final String id;
	private final URL url;
	private final String title;
	private final String author;
	private final String version;
	private final Set<String> tags;
	private final float score;

	public DocshellResult(final String id, final URL url, final String title, final String author, final String version,
			final Set<String> tags, final float score) {
		this.id = id;
		this.url = url;
		this.title = title;
		this.author = author;
		this.version = version;
		this.tags = tags;
		this.score = score;
	}

	public String getId() {
		return this.id;
	}

	public URL getUrl() {
		return this.url;
	}

	public String getTitle() {
		return this.title;
	}

	public String getAuthor() {
		return this.author;
	}

	public String getVersion() {
		return this.version;
	}

	public Set<String> getTags() {
		return this.tags;
	}

	public float getScore() {
		return this.score;
	}
}
