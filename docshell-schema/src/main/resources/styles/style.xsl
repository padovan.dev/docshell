<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:x="docshell.gitlab.io/schema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<head>
				<style>
					:root {
						font-family: arial;
						color: #444;
						--io-background-color: #f1f1f1;
						--io-text-color: #444;
					}
					.filecontent-container {
						position: relative;
					}
					.filecontent-container > div {
						position: absolute;
						right: 20px;
						bottom: 20px;
						opacity: 0;
						transition: opacity .5s ease-out;
						-moz-transition: opacity .5s ease-out;
						-webkit-transition: opacity .5s ease-out;
						-o-transition: opacity .5s ease-out;
						background-color: #E5E5E5;
						padding: 5px;
					}
					.filecontent-container:hover > div {
						opacity: 1;
					}
					.configuration {
						background-color: #FAFAFA;
						margin-top: 10px;
						margin-bottom: 10px;
						padding: 10px;
					}
					.configuration > .title {
						font-size: larger;
						font-weight: bold;
						padding-bottom: 5px;
					}
					.configuration > .comment {
						font-size: medium;
						padding-left: 10px;
						padding-right: 10px;
					}
					.configuration > .title, .command {
						padding-left: 5px;
						padding-right: 5px;
					}
					.command > .comment {
						margin-top: 10px;
						margin-bottom: 10px;
						font-size: small;
					}
					.command > .io {
						font-family: monospace;
						background-color: var(--io-background-color);
						color: var(--io-text-color);
						padding: 5px;
						font-size: 14px;
						line-height: 1.5;
					}
					.command > .io .comment {
						font-family: arial;
						margin-top: 5px;
						margin-bottom: 5px;
						font-size: small;
					}
					.command > .io .input {
					}
					.command > .io .filecontent::selection,
					.command > .io .filecontent::-moz-selection,
					.command > .io .input::selection,
					.command > .io .input::-moz-selection {
						color: var(--io-background-color);
						background-color: var(--io-text-color);
					}
					.command > .io .output {
						white-space: pre-wrap;
						color: gray;
					}
					.command > .io .filecontent {
						font-size: 14px;
						white-space: pre-wrap;
						color: gray;
						border: 2px dashed #DDD;
						margin-top: 10px;
						margin-bottom: 10px;
						padding: 10px;
						display: inline-block;
						cursor: pointer;
						width: 100%;
						resize: vertical;
						box-sizing: border-box;
					}
					.filecontent-container > div,
					.command > .io .comment,
					.command > .comment,
					.command > .io .prompt,
					.command > .io .output {
						-webkit-touch-callout: none;
						-webkit-user-select: none;
						-khtml-user-select: none;
						-moz-user-select: none;
						-ms-user-select: none;
						user-select: none;
					}
					.command > .io .prompt > .server {
						font-weight: bold;
					}
					.server.db {
						color: red;
					}
					.server.bash {
						color: limegreen;
					}
					.server.bash.other {
						color: royalblue;
					}
				</style>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.9.0/showdown.min.js" />
				<script src="https://unpkg.com/mermaid@7.1.0/dist/mermaid.js" />
				<script>
					function selectContent(event) {
						event.target.select();
						document.execCommand('copy');
					}
					function selectAllText(element) {
						let range = document.createRange();
						range.selectNode(element);
						window.getSelection().removeAllRanges();
						window.getSelection().addRange(range);
					}
					function init() {
						let b64s = document.getElementsByClassName('b64');
						Array.prototype.forEach.call(b64s, element => {element.innerHTML = atob(element.innerHTML);});
						
						let mds = document.getElementsByClassName('md');
						let converter = new showdown.Converter();
						Array.prototype.forEach.call(mds, element => {
						console.log(element.innerText);
							html = converter.makeHtml(element.innerText);
							element.innerHTML = html;
						});
						
						let areas = document.getElementsByTagName('textarea');
						Array.prototype.forEach.call(areas, textArea => {let h=textArea.scrollHeight+10;textArea.style.height = 'auto'; textArea.style.height = h + 'px';});
					}
				</script>
			</head>
			<body onload="init()">
				<h2>
					<xsl:if test="x:server/x:title">
						<xsl:value-of select="x:server/x:title" />
					</xsl:if>
					@<xsl:value-of select="x:server/x:name" />
				</h2>
				<xsl:for-each select="x:server/x:configurations">
					<p class="title">
						configurazione del 
						<xsl:value-of select="x:date" />
					</p>
					<xsl:for-each select="x:configuration">
						<div class="configuration">
							<div class="title">
								⚙ <a name="ciao">
								<xsl:value-of select="x:title" />
								</a>
							</div>
							<xsl:if test="x:comment">
								<div class="comment md">
									<xsl:value-of select="x:comment" disable-output-escaping="yes" />
								</div>
							</xsl:if>
							<xsl:for-each select="x:command">
								<div class="command">
									<xsl:if test="x:comment">
										<div class="comment md">
											<xsl:value-of select="x:comment" disable-output-escaping="yes" />:
										</div>
									</xsl:if>
									<div class="io">
										<xsl:for-each select="x:io">
											<xsl:if test="x:comment">
												<div class="comment">
													&gt;&gt; <xsl:value-of select="x:comment" />:
												</div>
											</xsl:if>
							
											<xsl:if test="@type='input' and position() = 1">
												<xsl:apply-templates select="../x:shellType" />
											</xsl:if>
											<xsl:if test="@type='input'">
												<span class="input" ondblclick="selectAllText(this)">
													<xsl:value-of select="x:value" />
													<br/>
												</span>
											</xsl:if>
											<xsl:if test="@type='output'">
												<span class="output">
													<xsl:value-of select="x:value" />
												</span>
											</xsl:if>
											<xsl:if test="@type='base64'">
												<div class="filecontent-container">
													<textarea readonly="true" class="filecontent b64" onclick="selectContent(event);">
														<xsl:value-of select="x:value" />
													</textarea>
													<div>clicca per copiare</div>
												</div>
											</xsl:if>
											<xsl:if test="@type='diagram'">
												<img>
												ciao
												</img>
												<div class="mermaid">
													<xsl:value-of select="x:value" />
												</div>
											</xsl:if>
											<xsl:if test="@type='filecontent'">
												<div class="filecontent-container">
													<textarea readonly="true" class="filecontent" onclick="selectContent(event);">
														<xsl:value-of select="x:value" />
													</textarea>
													<div>clicca per copiare</div>
												</div>
											</xsl:if>
										</xsl:for-each>
									</div>
								</div>
							</xsl:for-each>
						</div>
					</xsl:for-each>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>


<xsl:template match="x:shellType[text() = 'POSTGRESQL']">
	<span class="prompt"><span class="server db"><xsl:value-of select="../x:property[@key='database']" /></span><xsl:value-of select="../x:property[@key='prompt']" /> </span>
</xsl:template>

<xsl:template match="x:shellType[text() = 'MYSQL']">
	<span class="prompt"><span class="server db">mysql</span>&gt; </span>
</xsl:template>

<xsl:template match="x:shellType[text() = 'BASH']">
	<xsl:choose>
		<xsl:when test="(/x:server/x:name = ../x:property[@key='serverName']) or not(../x:property[@key='serverName']) or (../x:property[@key='serverName'] = '')" >
		<span class="prompt"><span class="server bash"><xsl:value-of select="../x:property[@key='user']" />@<xsl:value-of select="/x:server/x:name" /></span>:<xsl:value-of select="../x:property[@key='wd']" />$ </span>
		</xsl:when>
		<xsl:otherwise>
		<span class="prompt"><span class="server bash other"><xsl:value-of select="../x:property[@key='user']" />@<xsl:value-of select="../x:property[@key='serverName']" /></span>:<xsl:value-of select="../x:property[@key='wd']" />$ </span>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
</xsl:stylesheet> 
