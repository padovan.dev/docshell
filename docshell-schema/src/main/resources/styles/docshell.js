var params = [];
var currentHighlightId;
function selectContent(event) {
    event.target.select();
    document.execCommand('copy');
}
function selectAllText(element,id) {
    let range = document.createRange();
    range.selectNode(element);
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(range);
    document.execCommand('copy');
    window.location.hash='#'+id;
}
function init() {
    let b64s = document.getElementsByClassName('b64');
    Array.prototype.forEach.call(b64s, element => {element.innerHTML = atob(element.innerHTML);});
    
    let mds = document.getElementsByClassName('md');
    let converter = new showdown.Converter();
    Array.prototype.forEach.call(mds, element => {
        console.log(element.innerText);
        html = converter.makeHtml(element.innerText);
        element.innerHTML = html;
    });
    
    let areas = document.getElementsByTagName('textarea');
    Array.prototype.forEach.call(areas, textArea => {let h=textArea.scrollHeight+10;textArea.style.height = 'auto'; textArea.style.height = h + 'px';});
    
    replaceParam();

    if (params.length) {
        var el = document.getElementById('param-list-form');
        let tbl = document.createElement('table');
        el.appendChild(tbl);
        for (i=0;i<params.length; i++) {
            let tblRow=document.createElement('tr');
            tbl.appendChild(tblRow);
            let p=params[i];
            let tblCell=document.createElement('td');
            tblRow.appendChild(tblCell);
            tblCell.appendChild(document.createTextNode('📌 ' + p.description));
            tblCell=document.createElement('td');
            tblRow.appendChild(tblCell);
            let paramInput = document.createElement('input');
            paramInput.name=p.name;
            paramInput.value = paramValue(p.name);
            tblCell.appendChild(paramInput);
        }
        let submit = document.createElement('input');
        submit.value = 'Imposta parametri';
        submit.type = 'submit';
        el.appendChild(submit);
    }
    highlight();
    window.onhashchange=highlight;
}
function highlight() {
    let hash = window.location.hash;
    if (currentHighlightId) {
        let el = document.getElementById(currentHighlightId);
        if (el) {
            el.classList.remove('hl');
        }
    }
    if (hash) {
        hash=hash.substr(1);
        currentHighlightId=hash;
        console.log(hash);
        let el = document.getElementById(hash);
        console.log(el);
        if (el) {
            el.classList.add('hl');
        }
    }
}
function paramValue(name) {
    return new URL(window.location.href).searchParams.get(name);
}
function replaceParam() {
    var classes = ["server", "output","input", "comment"];
    for (i = 0; i < classes.length; i++) {
        var elements = document.getElementsByClassName(classes[i]);
        for(j = 0; j < elements.length; j++) {
            for(k=0; k < params.length; k++) {
                replaceAllInElement(params[k], elements[j]);
            }
        }
    }
}
function replaceAllInElement(param, element) {
    re = new RegExp("{" + param.name + "}", "g");
    newValue = paramValue(param.name) || param.defaultValue || param.name;
    element.innerHTML =element.innerHTML.replace(re, newValue);
}