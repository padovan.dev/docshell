package cloud.docshell.cli;

import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import com.beust.jcommander.JCommander;

import cloud.docshell.cli.importer.line.LineParseException;
import cloud.docshell.cli.parser.AppendCommand;
import cloud.docshell.cli.parser.CommonConfig;
import cloud.docshell.cli.parser.CreateCommand;
import cloud.docshell.cli.parser.ExportCommand;

public class DocShell {

	public static void main(final String[] args) {
		final CreateCommand createCommand = new CreateCommand();
		final AppendCommand appendCommand = new AppendCommand();
		final ExportCommand exportCommand = new ExportCommand();
		final CommonConfig config2 = new CommonConfig();
		final JCommander jcommander = JCommander.newBuilder()
				.addObject(config2)
				.addCommand(appendCommand)
				.addCommand(createCommand)
				.addCommand(exportCommand)
				.build();
		jcommander.parse(args);

		if (config2.isPrintVersion()) {
			// TODO
			return;
		}

		if (config2.isPrintHelp()) {
			// TODO
			return;
		}

		final String command = jcommander.getParsedCommand();
		if (command == null) {
			// TODO
			return;
		}
		try {
			switch (command) {
				case CreateCommand.NAME:
					createCommand.exec();
					break;
				case AppendCommand.NAME:
					appendCommand.exec();
					break;
				case ExportCommand.NAME:
					break;
				default:
					break;
			}
		} catch (IOException | LineParseException | JAXBException | XMLStreamException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
