package cloud.docshell.cli.importer;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import cloud.docshell.cli.importer.command.CommandSelector;
import cloud.docshell.cli.importer.line.LineParseException;
import cloud.docshell.cli.importer.line.LineParser;
import cloud.docshell.schema.docshell.Command;
import cloud.docshell.schema.docshell.Docshell;
import cloud.docshell.schema.docshell.Io;
import cloud.docshell.schema.docshell.IoType;

public class CommandParser {

	private static final String SCRIPT_STARTED_ON = "Script started on ";

	private final Docshell docshell;

	public CommandParser(final Docshell docshell) {
		this.docshell = docshell;
	}

	public void parse(final LineParser parser, final InputStream inputStream, final CommandSelector... selectors) throws LineParseException {
		final List<Command> commands = this.read(parser, inputStream, selectors);
		this.docshell.getCommand().addAll(commands);
	}

	private List<Command> read(final LineParser parser, final InputStream inputStream, final CommandSelector[] selectors) throws LineParseException {
		final List<String> lines = parser.lines(inputStream);
		final List<Command> commands = new ArrayList<>();
		Command command = null;
		Io lastOutput = null;
		for (final String line : lines) {
			final Command curentCommand = getCommand(line, selectors);
			if (curentCommand != null) {
				command = curentCommand;
				commands.add(command);
				lastOutput = null;
			} else if (command != null) {
				if (lastOutput == null) {
					lastOutput = new Io();
					command.getIo().add(lastOutput);
				}
				lastOutput.setType(IoType.OUTPUT);
				final String value = lastOutput.getValue();
				lastOutput.setValue(value == null ? line : value + "\n" + line);
			} else if (line.startsWith(SCRIPT_STARTED_ON) && this.docshell.getDate() == null) {
				final GregorianCalendar gregorianCalendar = CommandParser.getScriptDate(line);
				if (gregorianCalendar != null) {
					this.docshell.setDate(OffsetDateTime.ofInstant(gregorianCalendar.toInstant(), ZoneId.systemDefault()));
				}

			}
		}
		return commands;
	}

	private static GregorianCalendar getScriptDate(final String line) {
		final String dateS = line.substring(SCRIPT_STARTED_ON.length(), Math.min(SCRIPT_STARTED_ON.length() + 25, line.length()));
		final SimpleDateFormat sdf;
		final int length = dateS.length();
		if (length == 25) {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssXXX");
		} else if (length == 24) {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssXX");
		} else {
			return null;
		}
		try {
			final Date date = sdf.parse(dateS);
			final GregorianCalendar gregorianCalendar = new GregorianCalendar();
			gregorianCalendar.setTime(date);
			return gregorianCalendar;
		} catch (final Exception ex) {
			System.err.println(ex.getMessage());
			return null;
		}
	}

	private static Command getCommand(final String line, final CommandSelector[] selectors) {
		for (final CommandSelector selector : selectors) {
			final Command command = selector.getCommand(line);
			if (command != null) {
				return command;
			}
		}
		return null;

	}

}