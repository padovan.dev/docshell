package cloud.docshell.cli.importer;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.ezix.Nodeinfo;

import cloud.docshell.schema.server.Disk;
import cloud.docshell.schema.server.Hardware;
import cloud.docshell.schema.server.ObjectFactory;
import cloud.docshell.schema.server.Server;

public class LshwConverter {

	private final ObjectFactory objectFactory = new ObjectFactory();

	public Server create(final InputStream is) throws JAXBException, XMLStreamException {
		final Nodeinfo nodeinfo = LshwConverter.getNodeinfo(is);
		return this.create(nodeinfo);
	}

	public Server create(final Nodeinfo nodeinfo) {

		final Server server = this.objectFactory.createServer();
		final String name = nodeinfo.getId();
		server.setName(name);
		this.add(nodeinfo, server);
		return server;
	}

	public void add(final InputStream is, final Server server) throws FactoryConfigurationError, XMLStreamException, JAXBException {
		final Nodeinfo nodeinfo = LshwConverter.getNodeinfo(is);
		this.add(nodeinfo, server);
	}

	public void add(final Nodeinfo nodeinfo, final Server server) {
		this.setHardware(nodeinfo, server, LocalDateTime.now().atOffset(ZoneOffset.UTC));

	}

	private static Nodeinfo getNodeinfo(final InputStream is) throws FactoryConfigurationError, XMLStreamException, JAXBException {
		final XMLInputFactory xif = XMLInputFactory.newFactory();
		xif.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.FALSE);
		final StreamSource xml = new StreamSource(is);
		final XMLStreamReader xsr = xif.createXMLStreamReader(xml);
		xsr.nextTag();
		while (!xsr.getLocalName().equals("node")) {
			xsr.nextTag();
		}
		final JAXBContext jc = JAXBContext.newInstance(Nodeinfo.class);
		final Unmarshaller unmarshaller = jc.createUnmarshaller();
		final JAXBElement<Nodeinfo> jb = unmarshaller.unmarshal(xsr, Nodeinfo.class);
		xsr.close();
		final Nodeinfo nodeinfo = jb.getValue();
		return nodeinfo;
	}

	private void setHardware(final Nodeinfo nodeinfo, final Server server, final OffsetDateTime date) {
		final Hardware hardware = this.objectFactory.createHardware();
		server.getHardware().add(hardware);
		hardware.setDate(date);

		final Nodeinfo nodeCoreMemory = this.getNode(nodeinfo, "core", "memory");
		System.out.println(nodeCoreMemory);
		if (nodeCoreMemory != null) {
			// TODO verify units nodeCoreMemory.getSize().getUnits()
			hardware.setMemory(nodeCoreMemory.getSize().getValue().longValue());
		}
		final Nodeinfo nodeCoreCpu = this.getNode(nodeinfo, "core", "cpu");
		if (nodeCoreCpu != null) {
			// TODO multi cpu
			hardware.getCpu().add(nodeCoreCpu.getProduct());
		}
		final Nodeinfo nodeCore = this.getNode(nodeinfo, "core");
		for (final Nodeinfo nodeCoreChild : nodeCore.getNode()) {
			if (nodeCoreChild.getId().startsWith("scsi")) {
				final Nodeinfo nodeDisk = this.getNode(nodeCoreChild, "disk");
				if (nodeDisk != null) {
					final Disk disk = this.objectFactory.createDisk();
					hardware.getDisk().add(disk);
					disk.setDevice(nodeDisk.getProduct());
					// TODO verify units
					disk.setSize(nodeDisk.getSize().getValue().longValue());
					disk.setType(nodeDisk.getClazz());
				}
			}
		}
	}

	private Nodeinfo getNode(final Nodeinfo nodeinfo, final String... ids) {
		return this.getNode(nodeinfo, 0, ids);
	}

	private Nodeinfo getNode(final Nodeinfo nodeinfo, final int depth, final String... ids) {
		if (ids.length == depth) {
			return nodeinfo;
		}
		final String id = ids[depth];
		for (final Nodeinfo nodeinfoChild : nodeinfo.getNode()) {
			if (nodeinfoChild.getId().equals(id)) {
				return this.getNode(nodeinfoChild, depth + 1, ids);
			}
		}
		return null;
	}
}
