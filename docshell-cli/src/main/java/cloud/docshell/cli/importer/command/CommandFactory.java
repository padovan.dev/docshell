package cloud.docshell.cli.importer.command;

public class CommandFactory {

	public static final CommandSelector BASH = new RegexCommandSelector("BASH", "^(.+)@(.+):(.+)[\\$#] (.+)", Integer.valueOf(4), null, "user", "serverName", "wd");
	public static final CommandSelector POSTGRESQL = new RegexCommandSelector("POSTGRESQL", "^(.+)([\\(=-][>#])(.*)", Integer.valueOf(3), null, "database", "prompt");
	public static final CommandSelector MYSQL = new RegexCommandSelector("MYSQL", "^mysql>(.*)", Integer.valueOf(1));
}
