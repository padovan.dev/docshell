package cloud.docshell.cli.importer.command;

import cloud.docshell.schema.docshell.Command;

public interface CommandSelector {

	Command getCommand(String line);
}
