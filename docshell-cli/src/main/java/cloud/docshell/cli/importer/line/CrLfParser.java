package cloud.docshell.cli.importer.line;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CrLfParser {

	private static final char LF = 10;
	private static final char CR = 13;

	/**
	 * Parse lines and remove lines ends only with CR
	 *
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static List<String> visibleLine(final InputStreamReader is) throws IOException {
		final List<String> lines = new ArrayList<>();
		StringBuilder line = new StringBuilder();
		final char[] buf = new char[1024];
		int len;
		boolean cr = false;
		while ((len = is.read(buf)) > 0) {
			for (int i = 0; i < len; i++) {
				final char c = buf[i];

				if (c == LF) {
					lines.add(line.toString());
					line = new StringBuilder();
					cr = false;
				} else if (c == CR) {
					cr = true;
				} else {
					if (cr) {
						line = new StringBuilder();
						cr = false;
					}
					line.append(c);
				}
			}
		}
		lines.add(line.toString());
		return lines;
	}
}
