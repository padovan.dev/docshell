package cloud.docshell.cli.importer.line;

public class LineParseException extends Exception {

	private static final long serialVersionUID = 1910653112445497156L;

	public LineParseException() {
		super();
	}

	public LineParseException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public LineParseException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public LineParseException(final String message) {
		super(message);
	}

	public LineParseException(final Throwable cause) {
		super(cause);
	}

}
