package cloud.docshell.cli.parser;

import com.beust.jcommander.Parameters;

@Parameters(commandNames = ExportCommand.NAME, commandDescription = "export xml to html")
public class ExportCommand {

	public final static String NAME = "export";
}
