package cloud.docshell.cli.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLStreamException;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import cloud.docshell.cli.importer.CommandParser;
import cloud.docshell.cli.importer.LshwConverter;
import cloud.docshell.cli.importer.command.CommandFactory;
import cloud.docshell.cli.importer.command.CommandSelector;
import cloud.docshell.cli.importer.line.LineParseException;
import cloud.docshell.cli.importer.line.XTermParser;
import cloud.docshell.schema.docshell.Docshell;
import cloud.docshell.schema.docshell.ObjectFactory;
import cloud.docshell.schema.server.Server;

@Parameters(commandNames = CreateCommand.NAME, commandDescription = "create a new file with content")
public class CreateCommand {

	public final static String NAME = "create";

	@Parameter(names = "server")
	private boolean server;

	@Parameter(names = "docshell")
	private boolean docshell;

	// docshell and server
	@Parameter(names = { "-o", "--output-file" })
	private File outputFile;

	// docshell only
	@Parameter(names = { "-s", "--script-file" }, variableArity = true)
	private List<File> scriptFiles;

	@Parameter(names = { "-t", "--title" })
	private String title;

	@Parameter(names = { "-a", "--author" })
	private String author;

	@Parameter(names = { "-d", "--date" })
	private Date date;

	@Parameter(names = { "-V", "--docshell-version" })
	private String version;

	@Parameter(names = { "--shell-type" }, variableArity = true)
	private List<String> shellTypes;

	@Parameter(names = { "-c", "--config-file" })
	private File configFile;

	// server only
	@Parameter(names = { "--lshw" })
	private File lshwFile;

	// TODO
	private void validate() {
		if (this.server && this.docshell) {
			throw new ParameterException("Specify just one between server and docshell");
		}
		// TODO
	}

	public void exec() throws IOException, LineParseException, JAXBException, XMLStreamException {
		this.validate();
		if (this.docshell) {
			this.docshell();
			return;
		}
		if (this.server) {
			if (this.lshwFile != null) {
				this.lshw();
				return;
			}
		}
		// TODO
		throw new UnsupportedOperationException("not yet implemented");
	}

	private void lshw() throws JAXBException, XMLStreamException, IOException {
		final LshwConverter lshwConverter = new LshwConverter();
		try (InputStream is = new FileInputStream(this.lshwFile)) {
			final Server serverObj = lshwConverter.create(is);

			try (OutputStream os = this.outputFile == null ? System.out : new FileOutputStream(this.outputFile)) {
				final JAXBContext jaxbContext = JAXBContext.newInstance(Server.class);
				final Marshaller marshaller = jaxbContext.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				marshaller.marshal(serverObj, os);
			}
		}
	}

	private void docshell() throws IOException, LineParseException, JAXBException {
		final ObjectFactory of = new ObjectFactory();
		final Docshell ds = of.createDocshell();
		if (this.title != null) {
			ds.setTitle(this.title);
		}
		if (this.author != null) {
			ds.setAuthor(this.author);
		}

		if (this.date != null) {
			ds.setDate(OffsetDateTime.ofInstant(this.date.toInstant(), ZoneId.systemDefault()));
		}
		if (this.version != null) {
			ds.setVersion(this.version);
		}
		final CommandParser commandParser = new CommandParser(ds);
		final XTermParser xTermParser = new XTermParser();
		final CommandSelector[] selectors = { CommandFactory.BASH, CommandFactory.MYSQL, CommandFactory.POSTGRESQL }; // TODO
		if (this.scriptFiles == null) {
			commandParser.parse(xTermParser, System.in, selectors);
		} else {
			for (final File scriptFile : this.scriptFiles) {
				try (FileInputStream fis = new FileInputStream(scriptFile)) {
					commandParser.parse(xTermParser, fis, selectors);
				}
			}
		}
		try (OutputStream os = this.outputFile == null ? System.out : new FileOutputStream(this.outputFile)) {
			final JAXBContext jaxbContext = JAXBContext.newInstance(Docshell.class);
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(ds, os);
		}
	}
}
