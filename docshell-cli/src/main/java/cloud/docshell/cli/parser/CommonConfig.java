package cloud.docshell.cli.parser;

import com.beust.jcommander.Parameter;

public class CommonConfig {

	@Parameter(names = "--help", help = true)
	private boolean printHelp;
	@Parameter(names = "--version", hidden = true)
	private boolean printVersion;

	public boolean isPrintHelp() {
		return this.printHelp;
	}

	public boolean isPrintVersion() {
		return this.printVersion;
	}
}