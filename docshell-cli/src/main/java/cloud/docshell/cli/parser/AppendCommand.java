package cloud.docshell.cli.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import cloud.docshell.cli.importer.CommandParser;
import cloud.docshell.cli.importer.command.CommandFactory;
import cloud.docshell.cli.importer.command.CommandSelector;
import cloud.docshell.cli.importer.line.LineParseException;
import cloud.docshell.cli.importer.line.XTermParser;
import cloud.docshell.schema.docshell.Docshell;

@Parameters(commandNames = AppendCommand.NAME, commandDescription = "append data to existing file")
public class AppendCommand {

	public final static String NAME = "append";

	@Parameter(names = "server")
	private boolean server;

	@Parameter(names = "docshell")
	private boolean docshell;

	// docshell and server
	@Parameter(names = { "-o", "--output-file" }, required = true)
	private File outputFile;

	// docshell only
	@Parameter(names = { "-s", "--script-file" }, variableArity = true)
	private List<File> scriptFiles;

	@Parameter(names = { "--shell-type" }, variableArity = true)
	private List<String> shellTypes;

	@Parameter(names = { "-c", "--config-file" })
	private File configFile;

	// server only
	// TODO
	private void validate() {
		if (this.server && this.docshell) {
			throw new ParameterException("Specify just one between server and docshell");
		}

		// TODO
	}

	public void exec() throws IOException, LineParseException, JAXBException {
		this.validate();
		if (this.docshell) {
			this.docshell();
			return;
		}
		// TODO
		throw new UnsupportedOperationException("not yet implemented");
	}

	private void docshell() throws IOException, LineParseException, JAXBException {
		Docshell ds;
		final JAXBContext jaxbContext = JAXBContext.newInstance(Docshell.class);
		try (InputStream is = new FileInputStream(this.outputFile)) {
			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			ds = (Docshell) unmarshaller.unmarshal(is);
		}

		final CommandParser commandParser = new CommandParser(ds);
		final XTermParser xTermParser = new XTermParser();
		final CommandSelector[] selectors = { CommandFactory.BASH, CommandFactory.MYSQL, CommandFactory.POSTGRESQL }; // TODO
		if (this.scriptFiles == null) {
			commandParser.parse(xTermParser, System.in, selectors);
		} else {
			for (final File scriptFile : this.scriptFiles) {
				try (FileInputStream fis = new FileInputStream(scriptFile)) {
					commandParser.parse(xTermParser, fis, selectors);
				}
			}
		}
		try (OutputStream os = new FileOutputStream(this.outputFile)) {
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(ds, os);
		}
	}
}
