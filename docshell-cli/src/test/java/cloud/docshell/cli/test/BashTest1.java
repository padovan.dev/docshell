package cloud.docshell.cli.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Test;

import cloud.docshell.cli.DocShell;

public class BashTest1 {

	private final String TEST_OUTPUT_DIR = "test-output";

	@Test
	public void sshKey() {
		DocShell.main(new String[] { "create", "docshell", "-o", this.TEST_OUTPUT_DIR + "/ssh-key.xml", "-s", "src/test/resources/input/ssh-key" });
		assertTrue(Files.exists(Path.of(this.TEST_OUTPUT_DIR + "/ssh-key.xml")));
	}

	@Test
	public void bash1() throws IOException {
		final Path outputFile = Path.of(this.TEST_OUTPUT_DIR + "/bash1.xml");
		Files.deleteIfExists(outputFile);
		DocShell.main(new String[] { "create", "docshell", "-o", this.TEST_OUTPUT_DIR + "/bash1.xml", "-s", "src/test/resources/input/bash-script-1" });
		assertTrue(Files.exists(outputFile));
		DocShell.main(new String[] { "append", "docshell", "-o", this.TEST_OUTPUT_DIR + "/bash1.xml", "-s", "src/test/resources/input/bash-script-2" });
	}

}
